package com.udacity.travelapp;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.udacity.travelapp.network.OnPlaceResult;
import com.udacity.travelapp.network.PlaceDistanceNetworkRepository;
import com.udacity.travelapp.network.models.Element;
import com.udacity.travelapp.network.models.PlaceDistanceResponse;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    @BindView(R.id.nav_icon)
    ImageView navigationIconImageView;
    @BindView(R.id.car_icon)
    ImageView carIconImageView;
    @BindView(R.id.bike_icon)
    ImageView bikeIconImageView;
    @BindView(R.id.walk_icon)
    ImageView walkIconImageView;
    @BindView(R.id.train_icon)
    ImageView trainIconImageView;
    @BindView(R.id.distance_text_view)
    TextView distanceTextView;
    @BindView(R.id.time_text_view)
    TextView timeTextView;
    @BindView(R.id.reset_text_view)
    TextView resetTextView;
    @BindView(R.id.place_data_Layout)
    PercentRelativeLayout placeDataLayout;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private double mMyLatitude;
    private double mMyLongitude;
    private boolean isFirstTime;
    private Place selectedPlace = null;
    PlaceAutocompleteFragment autocompleteFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        isFirstTime = true;

        navigationIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPlace != null) {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=" + mMyLatitude + "," + mMyLongitude +
                                    "&daddr=" + selectedPlace.getLatLng().latitude + "," + selectedPlace.getLatLng().longitude));
                    startActivity(intent);
                }
            }
        });

        carIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePlaceDataInfoViews(1);
            }
        });

        bikeIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePlaceDataInfoViews(2);
            }
        });

        trainIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePlaceDataInfoViews(3);
            }
        });

        walkIconImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePlaceDataInfoViews(4);
            }
        });

        resetTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide the place data layout
                hideOverlayView();
                //reset text view data and images backgrounds
                changePlaceDataInfoViews(0);
                //clear the search view
                resetAutoCompleteText();
                //navigate map to my location
                addCurrentLocationMarkerToMap();
            }
        });
    }

    private void hideOverlayView() {
        placeDataLayout.animate()
                .translationY(0).alpha(0.0f)
                .translationYBy(placeDataLayout.getHeight())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        placeDataLayout.setVisibility(View.GONE);
                    }
                });
    }

    private void changePlaceDataInfoViews(int status) {
        if (selectedPlace != null) {
            switch (status) {

                case 0: // reset all views
                    distanceTextView.setText("Distance: N/A");
                    timeTextView.setText("Time: N/A");

                    carIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    carIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    walkIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    walkIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    bikeIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    bikeIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    trainIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    trainIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));
                    break;

                case 1: // car mode

                    getPlaceInformation(selectedPlace, CommonConstants.DRIVING_MODE);
                    carIconImageView.setBackgroundColor(getResources().getColor(R.color.buttons_color));
                    carIconImageView.setColorFilter(getResources().getColor(R.color.white));

                    bikeIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    bikeIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    trainIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    trainIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    walkIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    walkIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));
                    break;

                case 2: // bike mode
                    getPlaceInformation(selectedPlace, CommonConstants.BIKING_MODE);
                    bikeIconImageView.setBackgroundColor(getResources().getColor(R.color.buttons_color));
                    bikeIconImageView.setColorFilter(getResources().getColor(R.color.white));

                    carIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    carIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    trainIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    trainIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    walkIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    walkIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));
                    break;

                case 3: //transit mode
                    getPlaceInformation(selectedPlace, CommonConstants.TRANSIT_MODE);
                    trainIconImageView.setBackgroundColor(getResources().getColor(R.color.buttons_color));
                    trainIconImageView.setColorFilter(getResources().getColor(R.color.white));

                    carIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    carIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    bikeIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    bikeIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    walkIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    walkIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));
                    break;

                case 4: //walking mode
                    getPlaceInformation(selectedPlace, CommonConstants.WALKING_MODE);
                    walkIconImageView.setBackgroundColor(getResources().getColor(R.color.buttons_color));
                    walkIconImageView.setColorFilter(getResources().getColor(R.color.white));

                    carIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    carIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    bikeIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    bikeIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));

                    trainIconImageView.setBackgroundColor(getResources().getColor(R.color.white));
                    trainIconImageView.setColorFilter(getResources().getColor(R.color.buttons_color));
                    break;

                default:
                    break;

            }
        }
    }

    private void resetAutoCompleteText() {
        autocompleteFragment.setText("");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setupGoogleSearchWidget();
    }

    private void addCurrentLocationMarkerToMap(Place place) {
        mMap.clear();
        LatLng placeLatLng = place.getLatLng();
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLng(placeLatLng);
        mMap.addMarker(new MarkerOptions().position(placeLatLng).title(place.getName().toString()));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(8);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.moveCamera(yourLocation);
        mMap.animateCamera(zoom);
    }

    private void addCurrentLocationMarkerToMap() {
        mMap.clear();
        LatLng placeLatLng = new LatLng(mMyLatitude, mMyLongitude);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLng(placeLatLng);
        mMap.addMarker(new MarkerOptions().position(placeLatLng).title("Current location"));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(8);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.moveCamera(yourLocation);
        mMap.animateCamera(zoom);
    }

    private void setupGoogleSearchWidget() {
        if (isFirstTime) {
            addCurrentLocationMarkerToMap();
            isFirstTime = false;
        }
        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("Hesham", "Place: " + place.getName());
                selectedPlace = place;
                addCurrentLocationMarkerToMap(place);
                getPlaceInformation(place, CommonConstants.DRIVING_MODE);
                animatePlaceDataLayout();
                changePlaceDataInfoViews(1);
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Hesham", "An error occurred: " + status);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        final FragmentManager fragManager = this.getSupportFragmentManager();
        final Fragment mapFragment = fragManager.findFragmentById(R.id.map);
        final Fragment autoCompleteFragment = fragManager.findFragmentById(R.id.place_autocomplete_fragment);
        if (mapFragment != null && autoCompleteFragment != null) {
            fragManager.beginTransaction().remove(mapFragment).commit();
            fragManager.beginTransaction().remove(autoCompleteFragment).commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        checkLocationPermission();
    }

    private void findMyLocation() {
        // get location
        boolean canGetLocation = false;
        Location location = null;
        LocationManager locationManager = (LocationManager) this
                .getSystemService(this.LOCATION_SERVICE);

        // getting GPS status
        boolean isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        boolean isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
        } else {
            canGetLocation = true;
            if (isNetworkEnabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000L, 0, this);
                Log.d("dist", "LOC Network Enabled");
                if (locationManager != null) {
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                        Log.d("dist", "LOC by Network");
                        mMyLatitude = location.getLatitude();
                        mMyLongitude = location.getLongitude();
                    }
                }
            }
            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
                    Log.d("dist", "Activity: GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            Log.d("dist", "Activity: loc by GPS");

                            mMyLatitude = location.getLatitude();
                            mMyLongitude = location.getLongitude();
                        }
                    }
                }
            }
        }

    }


    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Request Location")
                        .setMessage("This application needs permission to use your location.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            findMyLocation();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        //Request location updates:
                        findMyLocation();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void animatePlaceDataLayout() {
        if (placeDataLayout.getVisibility() == View.GONE) {
            placeDataLayout.animate()
                    .translationY(placeDataLayout.getHeight()).alpha(1.0f)
                    .translationYBy(-placeDataLayout.getHeight())
                    .setDuration(500)
                    .setStartDelay(200)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            super.onAnimationStart(animation);
                            placeDataLayout.setVisibility(View.VISIBLE);
                            placeDataLayout.setAlpha(0.0f);
                        }
                    });
        }
    }

    private void getPlaceInformation(final Place place, String transportationMode) {
        String address = getCompleteAddressString(mMyLatitude, mMyLongitude);
        PlaceDistanceNetworkRepository repository = new PlaceDistanceNetworkRepository(this);
        repository.getPlaceDistance(address, place.getAddress().toString(), transportationMode,
                "EN-en", CommonConstants.API_KEY_VALUE, new OnPlaceResult() {
                    @Override
                    public void onSuccess(PlaceDistanceResponse placeDistanceResponse) {
                        if (placeDistanceResponse != null &&
                                placeDistanceResponse.getRows() != null &&
                                placeDistanceResponse.getRows().size() > 0 &&
                                placeDistanceResponse.getRows().get(0).getElements() != null &&
                                placeDistanceResponse.getRows().get(0).getElements().size() > 0) {

                            Element element = placeDistanceResponse.getRows().get(0).getElements().get(0);

                            if (element.distance != null && element.distance.getText() != null &&
                                    !element.distance.getText().isEmpty()) {
                                distanceTextView.setText("Distance: " + element.distance.getText());
                            } else {
                                distanceTextView.setText("Distance: N/A");
                            }

                            if (element.duration != null &&
                                    element.duration.getText() != null &&
                                    !element.duration.getText().isEmpty()) {
                                timeTextView.setText("Time: " + element.duration.getText());
                            } else {
                                timeTextView.setText("Time: N/A");
                            }
                        } else {
                            distanceTextView.setText("Distance: N/A");
                            timeTextView.setText("Time: N/A");
                        }

                    }

                    @Override
                    public void onFailure() {
                        Log.d("Hesham", "failed");
                    }
                });
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
//                Log.d("My Current loction address", "" + strReturnedAddress.toString());
            } else {
//                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
//            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }
}
