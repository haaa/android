package com.udacity.travelapp;

/**
 * Created by AmrNabil on 6/21/2017.
 */

public class CommonConstants {

    public static final String DRIVING_MODE = "driving";
    public static final String WALKING_MODE = "walking";
    public static final String BIKING_MODE = "bicycling";
    public static final String TRANSIT_MODE = "transit";
    public static final String BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/";
    public static final String ORIGINS_KEY = "origins";
    public static final String DESTINATIONS_KEY = "destinations";
    public static final String TRANSPORTATION_MODE_KEY = "mode";
    public static final String LANGUAGE_KEY = "language";
    public static final String API_KEY = "key";
    public static final String API_KEY_VALUE = "AIzaSyDL4q06tGqYFWPElgOBmYhZYAkelXnEVvQ";


}
