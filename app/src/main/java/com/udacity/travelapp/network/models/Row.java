package com.udacity.travelapp.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AmrNabil on 6/21/2017.
 */

public class Row implements Serializable {

    @SerializedName("elements")
    @Expose
    ArrayList<Element> elements;

    public Row() {
    }

    public ArrayList<Element> getElements() {
        return elements;
    }

    public void setElements(ArrayList<Element> elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        return "Row{" +
                "elements=" + elements +
                '}';
    }
}
