package com.udacity.travelapp.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by AmrNabil on 6/21/2017.
 */

public class PlaceDistanceResponse implements Serializable {

    @SerializedName("status")
    @Expose
    String status;

    @SerializedName("rows")
    @Expose
    ArrayList<Row> rows;

    public PlaceDistanceResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Row> getRows() {
        return rows;
    }

    public void setRows(ArrayList<Row> rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "PlaceDistanceResponse{" +
                "status='" + status + '\'' +
                ", rows=" + rows +
                '}';
    }
}
