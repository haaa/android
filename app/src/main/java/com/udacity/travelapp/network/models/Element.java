package com.udacity.travelapp.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AmrNabil on 6/21/2017.
 */

public class Element implements Serializable {

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("duration")
    @Expose
    public Result duration;

    @SerializedName("distance")
    @Expose
    public Result distance;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Result getDuration() {
        return duration;
    }

    public void setDuration(Result duration) {
        this.duration = duration;
    }

    public Result getDistance() {
        return distance;
    }

    public void setDistance(Result distance) {
        this.distance = distance;
    }

    public Element() {
    }

    @Override
    public String toString() {
        return "Element{" +
                "status='" + status + '\'' +
                ", duration=" + duration +
                ", distance=" + distance +
                '}';
    }
}
