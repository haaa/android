package com.udacity.travelapp.network;

import com.udacity.travelapp.network.models.PlaceDistanceResponse;

/**
 * Created by AmrNabil on 6/21/2017.
 */

public interface OnPlaceResult {

    void onSuccess(PlaceDistanceResponse placeDistanceResponse);

    void onFailure();
}
