package com.udacity.travelapp.network;

import android.content.Context;

import com.udacity.travelapp.CommonConstants;
import com.udacity.travelapp.NetworkUtils;
import com.udacity.travelapp.network.models.PlaceDistanceResponse;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by AmrNabil on 6/21/2017.
 */

public class PlaceDistanceNetworkRepository {

    public PlaceDistanceNetworkRepository(Context context) {
        provideApi(context);
    }

    public void getPlaceDistance(String origin, String destination, String transportationMode,
                                 String language, String apiKey, final OnPlaceResult onPlaceResult) {

        Call<PlaceDistanceResponse> placeDistanceResponseCall = sApiEndPointInterface.getPlaceDistance(origin, destination, transportationMode, language, apiKey);
        placeDistanceResponseCall.enqueue(new Callback<PlaceDistanceResponse>() {
            @Override
            public void onResponse(Call<PlaceDistanceResponse> call, Response<PlaceDistanceResponse> response) {
                if (response != null && response.isSuccessful() && response.body() != null) {
                    onPlaceResult.onSuccess(response.body());
                } else {
                    onPlaceResult.onFailure();
                }
            }

            @Override
            public void onFailure(Call<PlaceDistanceResponse> call, Throwable t) {
                onPlaceResult.onFailure();
            }
        });
    }

    private static ApiInterface sApiEndPointInterface;

    private static void setupApiEndPoint(final Context context) {

        int cacheSize = 50 * 1024 * 1024; // 50 MiB
        Cache cache = new Cache(context.getCacheDir(), cacheSize);

        OkHttpClient okHttpClient = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .cache(cache)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {

                        if (!NetworkUtils.isInternetConnection(context.getApplicationContext())) {
                            CacheControl.Builder cacheBuilder = new CacheControl.Builder();

                            cacheBuilder.maxStale(365, TimeUnit.DAYS);
                            cacheBuilder.onlyIfCached();
                            return chain.proceed(chain.request().newBuilder().cacheControl(cacheBuilder.build()).build());
                        }

                        return chain.proceed(chain.request());
                    }
                })
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        CacheControl.Builder cacheBuilder = new CacheControl.Builder();

                        if (NetworkUtils.isInternetConnection(context.getApplicationContext())) {

                            cacheBuilder.maxAge(10, TimeUnit.SECONDS);
                            okhttp3.Response response = chain.proceed(chain.request());
                            return response.newBuilder()
                                    .removeHeader("Pragma")
                                    .removeHeader("Cache-Control")
                                    .header("Cache-Control", cacheBuilder.build().toString())
                                    .build();
                        }

                        return chain.proceed(chain.request());
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(CommonConstants.BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create());

        sApiEndPointInterface = builder.client(okHttpClient).build().create(ApiInterface.class);
    }

    public static ApiInterface provideApi(Context context) {
        if (sApiEndPointInterface == null) {
            setupApiEndPoint(context);
        }

        return sApiEndPointInterface;
    }

}
