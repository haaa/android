package com.udacity.travelapp.network;

import com.udacity.travelapp.CommonConstants;
import com.udacity.travelapp.network.models.PlaceDistanceResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by AmrNabil on 6/21/2017.
 */

public interface ApiInterface {

    @GET("json")
    Call<PlaceDistanceResponse> getPlaceDistance(@Query(CommonConstants.ORIGINS_KEY) String origin,
                                                 @Query(CommonConstants.DESTINATIONS_KEY) String destination,
                                                 @Query(CommonConstants.TRANSPORTATION_MODE_KEY) String transportationMode,
                                                 @Query(CommonConstants.LANGUAGE_KEY) String language,
                                                 @Query(CommonConstants.API_KEY) String apiKey);

}
