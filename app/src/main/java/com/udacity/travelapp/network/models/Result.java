package com.udacity.travelapp.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AmrNabil on 6/21/2017.
 */

public class Result implements Serializable {

    @SerializedName("text")
    @Expose
    public String text;

    @SerializedName("value")
    @Expose
    public int value;

    public Result() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Result{" +
                "text='" + text + '\'' +
                ", value=" + value +
                '}';
    }
}
